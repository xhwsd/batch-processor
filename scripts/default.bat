@ECHO OFF
TITLE 批处理器缺省脚本 2021-4-14

ECHO 未匹配到处理脚本，使用缺省脚本处理...
ECHO.

ECHO 运行参数：%*
ECHO 目标文件：%1
SET FOLDER=%1
FOR /f %%a IN ("%FOLDER%") DO (
	SET FOLDER=%%~dpa
)
ECHO 目标目录：%FOLDER%
ECHO.

ECHO 批处理脚本文件：%0
ECHO 批处理运行目录：%cd%
ECHO 批处理文件目录：%~dp0
ECHO.

PAUSE
